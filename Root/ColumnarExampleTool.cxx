/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

/// @author Nils Krumnack

//
// includes
//

#include <ColumnarPrototype/ColumnarExampleTool.h>

#include <cmath>

//
// method implementations
//

namespace col
{
  ColumnarExampleTool ::
  ColumnarExampleTool (const std::string& name)
    : AsgTool (name)
  {}



  StatusCode ColumnarExampleTool ::
  initialize ()
  {
    return StatusCode::SUCCESS;
  }



  void ColumnarExampleTool ::
  calculateObject (ObjectId<ObjectType::muon> muon)
  {
    m_output[muon] = m_pt[muon] * cosh(m_eta[muon]);
  }



  void ColumnarExampleTool ::
  calculateRange (ObjectRange<ObjectType::muon> muons)
  {
    for (ObjectId<ObjectType::muon> muon : muons)
      m_output[muon] = m_pt[muon] * cosh(m_eta[muon]);
  }



  void ColumnarExampleTool ::
  calculateVector (ObjectRange<ObjectType::muon> muons)
  {
    auto pt = m_pt[muons];
    auto eta = m_eta[muons];
    auto output = m_output[muons];
    for (decltype(pt)::Index index = 0; index != pt.size(); ++ index)
      output[index] = pt[index] * cosh(eta[index]);
  }
}
