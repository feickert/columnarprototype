/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

/// @author Nils Krumnack



//
// includes
//

#include "CxxUtils/checker_macros.h"
ATLAS_NO_CHECK_FILE_THREAD_SAFETY;

#include <AsgTesting/UnitTest.h>
#include <AsgTools/AsgToolConfig.h>
#include <ColumnarPrototype/ColumnarExampleTool.h>
#include <xAODMuon/MuonAuxContainer.h>
#include <gtest/gtest.h>
#include <gtest/gtest-spi.h>

#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

//
// method implementations
//

namespace col
{
  namespace
  {
    /// \brief make a unique tool name to be used in unit tests
    std::string makeUniqueName ()
    {
      static std::atomic<unsigned> index = 0;
      return "unique" + std::to_string(++index);
    }
  }

  TEST (ColumnarMode0Test, object)
  {
    const std::string name = makeUniqueName();
    asg::AsgToolConfig config ("col::ColumnarExampleTool/" + name);
    std::shared_ptr<void> cleanup;
    ToolHandle<ColumnarExampleTool> tool;
    ASSERT_SUCCESS (config.makeTool (tool, cleanup));

    auto muon = std::make_unique<xAOD::Muon>();
    muon->makePrivateStore ();
    muon->setP4 (10e5, 1, 0);
    tool->calculateObject (muon.get());
    EXPECT_FLOAT_EQ (10e5*cosh(1), muon->auxdataConst<float>("output"));
  }


  TEST (ColumnarMode0Test, range)
  {
    const std::string name = makeUniqueName();
    asg::AsgToolConfig config ("col::ColumnarExampleTool/" + name);
    std::shared_ptr<void> cleanup;
    ToolHandle<ColumnarExampleTool> tool;
    ASSERT_SUCCESS (config.makeTool (tool, cleanup));

    auto muons = std::make_unique<xAOD::MuonContainer>();
    auto muonsAux = std::make_unique<xAOD::MuonAuxContainer>();
    muons->setStore (muonsAux.get());
    muons->push_back (new xAOD::Muon);
    muons->push_back (new xAOD::Muon);
    (*muons)[0]->setP4 (10e5, 1, 0);
    (*muons)[1]->setP4 (20e5, 2, 0);
    tool->calculateRange (muons.get());
    EXPECT_FLOAT_EQ (10e5*cosh(1), (*muons)[0]->auxdataConst<float>("output"));
    EXPECT_FLOAT_EQ (20e5*cosh(2), (*muons)[1]->auxdataConst<float>("output"));
  }


  TEST (ColumnarMode0Test, vector)
  {
    const std::string name = makeUniqueName();
    asg::AsgToolConfig config ("col::ColumnarExampleTool/" + name);
    std::shared_ptr<void> cleanup;
    ToolHandle<ColumnarExampleTool> tool;
    ASSERT_SUCCESS (config.makeTool (tool, cleanup));

    auto muons = std::make_unique<xAOD::MuonContainer>();
    auto muonsAux = std::make_unique<xAOD::MuonAuxContainer>();
    muons->setStore (muonsAux.get());
    muons->push_back (new xAOD::Muon);
    muons->push_back (new xAOD::Muon);
    (*muons)[0]->setP4 (10e5, 1, 0);
    (*muons)[1]->setP4 (20e5, 2, 0);
    tool->calculateVector (muons.get());
    EXPECT_FLOAT_EQ (10e5*cosh(1), (*muons)[0]->auxdataConst<float>("output"));
    EXPECT_FLOAT_EQ (20e5*cosh(2), (*muons)[1]->auxdataConst<float>("output"));
  }
}

ATLAS_GOOGLE_TEST_MAIN
