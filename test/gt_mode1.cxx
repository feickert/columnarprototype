/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

/// @author Nils Krumnack



//
// includes
//

#include "CxxUtils/checker_macros.h"
ATLAS_NO_CHECK_FILE_THREAD_SAFETY;

#include <AsgTesting/UnitTest.h>
#include <AsgTools/AsgToolConfig.h>
#include <ColumnarPrototype/ColumnarExampleTool.h>
#include <gtest/gtest.h>
#include <gtest/gtest-spi.h>

#pragma GCC diagnostic ignored "-Wdeprecated-declarations"

//
// method implementations
//

namespace col
{
  namespace
  {
    /// \brief make a unique tool name to be used in unit tests
    std::string makeUniqueName ()
    {
      static std::atomic<unsigned> index = 0;
      return "unique" + std::to_string(++index);
    }
  }

  TEST (ColumnarMode1Test, object)
  {
    // FIX ME: currently can't create the columnar tools via a
    // dictionary, this needs fixing in AsgTools.

    // const std::string name = makeUniqueName();
    // asg::AsgToolConfig config ("col::ColumnarExampleTool/" + name);
    // std::shared_ptr<void> cleanup;
    // ToolHandle<ColumnarExampleTool> tool;
    // ASSERT_SUCCESS (config.makeTool (tool, cleanup));
    auto tool = std::make_unique<ColumnarExampleTool>(makeUniqueName());

    std::vector<ColumnarOffsetType> offsets;
    offsets.push_back (0);
    offsets.push_back (1);
    tool->setColumn ("Muons", offsets.size(), offsets.data());
    std::vector<float> pt, eta, output;
    pt.push_back (10e5);
    eta.push_back (1);
    output.resize (pt.size(), 0.);
    tool->setColumn ("Muons_pt", pt.size(), pt.data());
    tool->setColumn ("Muons_eta", eta.size(), eta.data());
    tool->setColumn ("Muons_output", output.size(), output.data());
    tool->checkColumnsValid ();
    tool->calculateObject (ObjectId<ObjectType::muon>(0));
    EXPECT_FLOAT_EQ (10e5*cosh(1), output[0]);
  }



  TEST (ColumnarMode1Test, range)
  {
    // FIX ME: currently can't create the columnar tools via a
    // dictionary, this needs fixing in AsgTools.

    // const std::string name = makeUniqueName();
    // asg::AsgToolConfig config ("col::ColumnarExampleTool/" + name);
    // std::shared_ptr<void> cleanup;
    // ToolHandle<ColumnarExampleTool> tool;
    // ASSERT_SUCCESS (config.makeTool (tool, cleanup));
    auto tool = std::make_unique<ColumnarExampleTool>(makeUniqueName());

    std::vector<float> pt, eta, output;
    pt.push_back (10e5);
    pt.push_back (20e5);
    eta.push_back (1);
    eta.push_back (2);
    output.resize (pt.size(), 0.);
    tool->setColumn ("Muons_pt", pt.size(), pt.data());
    tool->setColumn ("Muons_eta", eta.size(), eta.data());
    tool->setColumn ("Muons_output", output.size(), output.data());
    std::vector<ColumnarOffsetType> offsets;
    offsets.push_back (0);
    offsets.push_back (pt.size());
    tool->setColumn ("Muons", offsets.size(), offsets.data());
    tool->checkColumnsValid ();
    tool->calculateRange (ObjectRange<ObjectType::muon>(0, offsets.back()));
    EXPECT_FLOAT_EQ (10e5*cosh(1), output[0]);
    EXPECT_FLOAT_EQ (20e5*cosh(2), output[1]);
  }



  TEST (ColumnarMode1Test, vector)
  {
    // FIX ME: currently can't create the columnar tools via a
    // dictionary, this needs fixing in AsgTools.

    // const std::string name = makeUniqueName();
    // asg::AsgToolConfig config ("col::ColumnarExampleTool/" + name);
    // std::shared_ptr<void> cleanup;
    // ToolHandle<ColumnarExampleTool> tool;
    // ASSERT_SUCCESS (config.makeTool (tool, cleanup));
    auto tool = std::make_unique<ColumnarExampleTool>(makeUniqueName());

    std::vector<float> pt, eta, output;
    pt.push_back (10e5);
    pt.push_back (20e5);
    eta.push_back (1);
    eta.push_back (2);
    output.resize (pt.size(), 0.);
    tool->setColumn ("Muons_pt", pt.size(), pt.data());
    tool->setColumn ("Muons_eta", eta.size(), eta.data());
    tool->setColumn ("Muons_output", output.size(), output.data());
    std::vector<ColumnarOffsetType> offsets;
    offsets.push_back (0);
    offsets.push_back (pt.size());
    tool->setColumn ("Muons", offsets.size(), offsets.data());
    tool->checkColumnsValid ();
    tool->calculateVector (ObjectRange<ObjectType::muon>(0, offsets.back()));
    EXPECT_FLOAT_EQ (10e5*cosh(1), output[0]);
    EXPECT_FLOAT_EQ (20e5*cosh(2), output[1]);
  }
}

ATLAS_GOOGLE_TEST_MAIN
