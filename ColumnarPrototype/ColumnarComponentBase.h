/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

/// @author Nils Krumnack


#ifndef COLUMNAR_PROTOTYPE_COLUMNAR_COMPONENT_BASE_H
#define COLUMNAR_PROTOTYPE_COLUMNAR_COMPONENT_BASE_H

#include <optional>
#include <stdexcept>
#include <string>
#include <unordered_map>

namespace col
{
  // TO DO: This type still needs to be adjusted to match whatever
  // uproot uses for its offset maps.
  using ColumnarOffsetType = std::size_t;


  class ColumnarComponentBase
  {
  public:
    virtual ~ColumnarComponentBase () = default;

#if COLUMNAR_ACCESS_MODE == 1u
    template<typename CT>
    void addColumn (const std::string& name, std::size_t& size, CT*& dataPtr, const std::string& offsetName) {
      if (columns.find (name) != columns.end())
        throw std::runtime_error ("duplicate column name: " + name);
      ColumnData data;
      data.sizePtr = &size;
      data.dataPtr = &reinterpret_cast<const void*&>(const_cast<const CT*&>(dataPtr));
      data.type = &typeid (std::decay_t<CT>);
      data.isConst = std::is_const_v<CT>;
      if (!offsetName.empty())
      {
        auto iter = columns.find (offsetName);
        if (iter == columns.end())
          throw std::logic_error ("unknown offset name: " + offsetName);
        if (*iter->second.type != typeid (ColumnarOffsetType))
          throw std::logic_error ("not an offset map: " + offsetName);
        data.offsets = &*iter;
      }
      columns.emplace (name, std::move (data));
    }

    template<typename CT>
    void setColumn (const std::string& name, std::size_t size, CT* dataPtr) {
      auto voidPtr = reinterpret_cast<const void*>(const_cast<const CT*>(dataPtr));
      setColumnVoid (name, size, voidPtr, typeid (std::decay_t<CT>), std::is_const_v<CT>);
    }

    void setColumnVoid (const std::string& name, std::size_t size, const void *dataPtr, const std::type_info& type, bool isConst) {
      auto column = columns.find (name);
      if (column == columns.end())
        throw std::runtime_error ("unknown column name: " + name);

      if (type != *column->second.type)
        throw std::runtime_error ("invalid type for column: " + name);
      if (isConst && !column->second.isConst)
        throw std::runtime_error ("assigning const vector to a non-const column: " + name);
      *column->second.sizePtr = size;
      *column->second.dataPtr = dataPtr;
    }

    void checkColumnsValid () const {
      for (auto& column : columns)
      {
        if (*column.second.dataPtr == nullptr)
          throw std::runtime_error ("uninitialized column: " + column.first);
      }
      std::optional<std::size_t> eventCount;
      for (auto& column : columns)
      {
        if (column.second.offsets != nullptr)
        {
          auto& offsets = *column.second.offsets;
          if (*offsets.second.sizePtr == 0u)
            throw std::runtime_error ("empty offset column: " + offsets.first);
          auto *offsetsPtr = static_cast<const ColumnarOffsetType*>
            (*offsets.second.dataPtr);
          if (*column.second.sizePtr != offsetsPtr[*offsets.second.sizePtr-1])
            throw std::runtime_error ("column size doesn't match offset column: " + column.first);
        } else
        {
          if (!eventCount.has_value())
            eventCount = *column.second.sizePtr;
          else if (eventCount.value() != *column.second.sizePtr)
            throw std::runtime_error ("inconsistent size for columns without offsets");
        }
      }
    }

  private:
    struct ColumnData final
    {
      std::size_t *sizePtr = nullptr;
      const void **dataPtr = nullptr;
      const std::type_info *type = nullptr;
      bool isConst = false;
      std::pair<const std::string,const ColumnData> *offsets = nullptr;
    };
    std::unordered_map<std::string,const ColumnData> columns;
#endif
  };
}

#endif
