/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

/// @author Nils Krumnack


#ifndef COLUMNAR_PROTOTYPE_OBJECT_ID_H
#define COLUMNAR_PROTOTYPE_OBJECT_ID_H

#include <ColumnarPrototype/ObjectType.h>

namespace col
{
  template<ObjectType O> class ObjectId final
  {
  public:

#if COLUMNAR_ACCESS_MODE == 0u

    ObjectId (const typename detail::ObjectTypeTraits<O>::xAODObject *val_object) noexcept
      : m_object (val_object)
    {}

    [[nodiscard]] const typename detail::ObjectTypeTraits<O>::xAODObject *getObject () const noexcept {
      return m_object;}

  private:
    const typename detail::ObjectTypeTraits<O>::xAODObject *m_object = nullptr;

#else

    explicit ObjectId (std::size_t val_index) noexcept
      : m_index (val_index)
    {}

    [[nodiscard]] std::size_t getIndex () const noexcept {
      return m_index;}

  private:
    std::size_t m_index = 0u;

#endif
  };
}

#endif
