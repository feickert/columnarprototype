/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

/// @author Nils Krumnack


#ifndef COLUMNAR_PROTOTYPE_READ_OBJECT_HANDLE_H
#define COLUMNAR_PROTOTYPE_READ_OBJECT_HANDLE_H

#include <ColumnarPrototype/ColumnarComponentBase.h>
#include <ColumnarPrototype/ObjectType.h>

namespace col
{
  template<ObjectType O> class ReadObjectHandle final
  {
  public:

    ReadObjectHandle (ColumnarComponentBase *owner,
                      const std::string& name)
      : m_name (name)
    {
      (void) owner;

#if COLUMNAR_ACCESS_MODE == 1u
      owner->addColumn (name, m_size, m_offsets, "");
#endif
    }

    const std::string& name () const noexcept {
      return m_name;}

#if COLUMNAR_ACCESS_MODE == 1u
    const ColumnarOffsetType *const* offsetsPtr () const noexcept {
      return &m_offsets;}
#endif

  private:

    std::string m_name;

#if COLUMNAR_ACCESS_MODE == 1u
    std::size_t m_size = 0u;
    ColumnarOffsetType *m_offsets = nullptr;
#endif
  };
}

#endif
