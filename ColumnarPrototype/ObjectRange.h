/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

/// @author Nils Krumnack


#ifndef COLUMNAR_PROTOTYPE_OBJECT_RANGE_H
#define COLUMNAR_PROTOTYPE_OBJECT_RANGE_H

#include <ColumnarPrototype/ObjectType.h>

namespace col
{
  template<ObjectType OT> class ObjectRangeIterator;

  template<ObjectType OT> class ObjectRange final
  {
  public:

    ObjectRangeIterator<OT> begin () const noexcept {
      return ObjectRangeIterator<OT> (this, m_beginIndex);}
    ObjectRangeIterator<OT> end () const noexcept {
      return ObjectRangeIterator<OT> (this, m_endIndex);}

    [[nodiscard]] std::size_t beginIndex () const noexcept {
      return m_beginIndex;}
    [[nodiscard]] std::size_t endIndex () const noexcept {
      return m_endIndex;}


#if COLUMNAR_ACCESS_MODE == 0u

    ObjectRange (const typename detail::ObjectTypeTraits<OT>::xAODContainer *val_container) noexcept
      : m_container (val_container),
        m_beginIndex (0), m_endIndex (m_container->size())
    {}

    [[nodiscard]] const typename detail::ObjectTypeTraits<OT>::xAODContainer *getContainer () const noexcept {
      return m_container;}

  private:
    const typename detail::ObjectTypeTraits<OT>::xAODContainer *m_container = nullptr;

#else

    explicit ObjectRange (std::size_t val_beginIndex,
                          std::size_t val_endIndex) noexcept
      : m_beginIndex (val_beginIndex), m_endIndex (val_endIndex)
    {}

#endif

  private:
    std::size_t m_beginIndex = 0u;
    std::size_t m_endIndex = 0u;
  };


  template<ObjectType OT> class ObjectRangeIterator final
  {
  public:

    ObjectRangeIterator (const ObjectRange<OT> *val_range,
                         std::size_t val_index) noexcept
      : m_range (val_range), m_index (val_index) {}

    ObjectId<OT> operator * () const noexcept {
#if COLUMNAR_ACCESS_MODE == 0u
      return ObjectId<OT> ((*m_range->getContainer())[m_index]);
#else
      return ObjectId<OT> (m_index);
#endif
    }

    ObjectRangeIterator<OT>& operator ++ () noexcept {
      ++ m_index; return *this;}

    bool operator == (const ObjectRangeIterator<OT>& that) const noexcept {
      return m_index == that.m_index;}
    bool operator != (const ObjectRangeIterator<OT>& that) const noexcept {
      return m_index != that.m_index;}

  private:
    const ObjectRange<OT> *m_range = nullptr;
    std::size_t m_index = 0u;
  };
}

#endif
