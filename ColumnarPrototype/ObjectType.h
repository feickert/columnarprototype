/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

/// @author Nils Krumnack


#ifndef COLUMNAR_PROTOTYPE_OBJECT_TYPE_H
#define COLUMNAR_PROTOTYPE_OBJECT_TYPE_H

#if COLUMNAR_ACCESS_MODE == 0u
#include <xAODMuon/MuonContainer.h>
#endif

namespace col
{
  enum class ObjectType
  {
    muon
  };

#if COLUMNAR_ACCESS_MODE == 0u
  namespace detail
  {
    template<ObjectType> struct ObjectTypeTraits;

    template<> struct ObjectTypeTraits<ObjectType::muon> final
    {
      using xAODContainer = xAOD::MuonContainer;
      using xAODObject = xAOD::Muon;
    };
  }
#endif
}

#endif
