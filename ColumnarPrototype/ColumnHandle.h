/*
  Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
*/

/// @author Nils Krumnack


#ifndef COLUMNAR_PROTOTYPE_COLUMN_HANDLE_H
#define COLUMNAR_PROTOTYPE_COLUMN_HANDLE_H

#include <AthContainers/AuxElement.h>
#include <ColumnarPrototype/ColumnarComponentBase.h>
#include <ColumnarPrototype/ObjectId.h>
#include <ColumnarPrototype/ObjectRange.h>
#include <ColumnarPrototype/ObjectType.h>
#include <ColumnarPrototype/ReadObjectHandle.h>

#include <Eigen/Dense>

namespace col
{
  namespace detail
  {
    template<typename T> struct ColumnTraits
    {
#if COLUMNAR_ACCESS_MODE == 0u
      using Accessor = SG::AuxElement::Decorator<T>;
#endif
      using Vector = Eigen::Map<Eigen::Matrix<T,Eigen::Dynamic,1>>;
    };

    template<typename T> struct ColumnTraits<const T>
    {
#if COLUMNAR_ACCESS_MODE == 0u
      using Accessor = SG::AuxElement::ConstAccessor<T>;
#endif
      using Vector = Eigen::Map<const Eigen::Matrix<T,Eigen::Dynamic,1>>;
    };
  }



  template<ObjectType OT,typename CT> class ColumnHandle final
  {
  public:

    ColumnHandle (ColumnarComponentBase *owner,
                  ReadObjectHandle<OT>& objectHandle,
                  const std::string& name)
#if COLUMNAR_ACCESS_MODE == 0u
      : m_accessor(name)
#endif
    {
      (void) owner;
      (void) objectHandle;

#if COLUMNAR_ACCESS_MODE == 1u
      owner->addColumn (objectHandle.name() + "_" + name, m_size, m_data, objectHandle.name());
#endif
    }


    CT& operator [] (ObjectId<OT> id) const noexcept
    {
#if COLUMNAR_ACCESS_MODE == 0u
      return m_accessor(*id.getObject());
#else
      return m_data[id.getIndex()];
#endif
    }


    typename detail::ColumnTraits<CT>::Vector operator [] (ObjectRange<OT> range) const noexcept
    {
#if COLUMNAR_ACCESS_MODE == 0u
      if (range.getContainer()->empty())
        return typename detail::ColumnTraits<CT>::Vector (nullptr, 0u);
      return typename detail::ColumnTraits<CT>::Vector (&m_accessor (*(*range.getContainer())[0]), range.getContainer()->size());
#else
      return typename detail::ColumnTraits<CT>::Vector (m_data+range.beginIndex(), range.endIndex()-range.beginIndex());
#endif
    }


  private:
#if COLUMNAR_ACCESS_MODE == 0u
    typename detail::ColumnTraits<CT>::Accessor m_accessor;
#else
    std::size_t m_size = 0u;
    CT *m_data = nullptr;
#endif
  };
}

#endif
